package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"idl2uml/converter"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	path := flag.String("path", "", "path to the IDL root folder, can be absolute and relative to execution folder, if not set it looks in the execution folder")
	message := flag.String("message", "", "message name (caps sensitive) to be used as root for class diagram (usable only in message diagram)")
	diagramRequested := flag.String("diagram", "package", "diagram type, [package | packageMessage | message] (caps ignored)")
	hiddenPackages := flag.String("hidden", "", "list of hidden packages or messages. Hidden dependencies will be displayed as fields and not aggregations / compositions in classes and packages will not be in the UML along with dependencies to them")
	packageToPackageConnect := flag.Bool("packageLinks", false, "defines if the package to package links are wanted in the output diagram, available only for packageMessage diagram, in package diagram it is always true")
	packageToDisplay := flag.String("package", "", "a single package to be displayed, all other will be hidden, works only with packageMessage diagram")
	outputPath := flag.String("o", "./diagram.puml", "output file path with extension")

	flag.Parse()

	*diagramRequested = strings.ToLower(*diagramRequested)

	// fix for empty path, as on some OS the filepath.Abs func does not return executable dir when given empty path
	if *path == "" {
		exePath, err := os.Getwd()
		check(err)
		*path = exePath
	}

	absolutePath, err := filepath.Abs(*path)
	check(err)

	var project converter.Project
	project.LoadFilesFromRoot(absolutePath)
	project.ParseFilesData()

	if *hiddenPackages != "" {
		hiddenPackagesSlice := strings.Split(*hiddenPackages, " ")
		project.HidePackagesAndMessages(hiddenPackagesSlice)
	}

	if *diagramRequested == "packagemessage" {
		project.SetHidePackageRelations(*packageToPackageConnect)

		if *packageToDisplay != "" {
			project.HideAllButGiven(*packageToDisplay)
		}
	} else {
		project.SetHidePackageRelations(true)
	}

	if len(project.ProtoFilesInStrings) == 0 {
		fmt.Println("No proto files found in directory did you set -path flag correctly?")
		return
	}

	outputFile, err := os.Create(*outputPath)
	check(err)

	switch *diagramRequested {
	case "packagemessage":
		fmt.Println("Generating package + message diagram")
		uml := project.GeneratePackageMessageDiagram()
		outputFile.WriteString(uml)
	case "package":
		fmt.Println("Generating package diagram")
		umlPackage := project.GeneratedPackageDiagram()
		outputFile.WriteString(umlPackage)
	case "message":
		if *message == "" {
			fmt.Println("ERROR No message name selected please use the -message flag to set the name of the message that is to be used as root for the diagram. (message name is case sensitive)")
			return
		}

		fmt.Println("Generating class diagram for message " + *message)
		umlMessage := project.GenerateUmlForMessage(*message)
		outputFile.WriteString(umlMessage)
	default:
		fmt.Println("No recognizable diagram type given (-diagram=).\nAvailable types:\n	-diagram=PackageMessage - Packages containing messages with connections between packages and messages\n	-diagram=Package - just the packages found in the path with their relations\n	-diagram=Message - needs -message also, it displays a tree with root the -message argument")
		return
	}

	outputFile.Sync()
	outputFile.Close()

	fmt.Println("Generation done, please see " + *outputPath)
}
