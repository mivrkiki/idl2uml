package converter

import (
	"fmt"
)

// Package contains parts of code and dependencies to other packages
type Package struct {
	Name                          string
	messageParts                  []messagePart
	dependsOn                     []*Package
	HasBeenGenerated              bool
	hiddenInUml                   bool
	GeneratePackageToPackageLinks bool
}

// GeneratePackageWithDependenciesInUML generates just the package and
// it's dependencies to other packages, internal elements are ignored
func (me Package) GeneratePackageWithDependenciesInUML() string {
	var uml string

	uml += "package " + me.Name + "{\n" + "}\n"

	for _, dependency := range me.dependsOn {
		uml += me.Name + " --> " + dependency.Name + "\n"
	}

	uml += "\n"

	return uml
}

// GeneratePackageAndMessagesInUml generates the package with internals and the package dependencies
func (me *Package) GeneratePackageAndMessagesInUml() string {
	var uml string

	if me.HasBeenGenerated || me.hiddenInUml {
		return ""
	}

	me.HasBeenGenerated = true

	for index := range me.dependsOn {
		packageToGenerate := me.dependsOn[index]
		uml += packageToGenerate.GeneratePackageAndMessagesInUml()
	}

	uml += "package " + me.Name + "{\n"

	for _, messagePartToGenerate := range me.messageParts {
		uml += messagePartToGenerate.generateUMLString()
	}

	uml += "}\n"

	for index := range me.dependsOn {
		dependency := me.dependsOn[index]
		uml += dependency.getConnectionUMLString(me.Name)
	}

	uml += "\n"

	return uml
}

// ConnectTo connects the called package to a dependency in case it is not already connected
// the package object needs to be in the list of available packages
func (me *Package) ConnectTo(nameOfPackage string, availablePackages *[]Package) {

	// cannot connect to self
	if me.Name == nameOfPackage {
		fmt.Println("WARNING: Package attempted to connect to itself. Package: " + me.Name)
		return
	}
	// search for a package with the same name
	for _, dependencyAlreadyPresent := range me.dependsOn {
		if dependencyAlreadyPresent.Name == nameOfPackage {
			// we allready depend on the given package no action needed
			return
		}
	}

	// Add the package as we failed to find a dependency with the name
	for i := 0; i < len(*availablePackages); i++ {
		if (*availablePackages)[i].Name == nameOfPackage {
			packageToDependOn := &((*availablePackages)[i])
			me.dependsOn = append(me.dependsOn, packageToDependOn)
		}
	}
}

// ConnectMessages connects all message parts in the package through the common interface
func (me *Package) ConnectMessages(packages *[]Package) {
	for i := 0; i < len(me.messageParts); i++ {
		(me.messageParts[i]).connectToDep(me, packages)
	}
}

// SetHideFlag sets a flag that is used in UML generation and it can hide completely the package and all parts inside if set to true
func (me *Package) SetHideFlag(hide bool) {
	me.hiddenInUml = hide

	for index := range me.messageParts {
		// we need to set all flags of the parts in the package
		part := me.messageParts[index]
		part.setHiddenParameter(hide)
	}
}

// HideMessage searches for and if found it hides the message part with the name provided
func (me *Package) HideMessage(nameToHide string) {
	for index := range me.messageParts {
		// we need to set all flags of the parts in the package
		part := me.messageParts[index]

		if part.getName() == nameToHide {
			part.setHiddenParameter(true)
		}
	}
}

// ResetGeneratedFlag resets the generated flag that is used in DFS generation, allowing for repeated generation
func (me *Package) ResetGeneratedFlag() {
	me.HasBeenGenerated = false

	for index := range me.messageParts {
		// we need to set all flags of the parts in the package
		part := me.messageParts[index]
		part.resetGeneratedFlag()
	}
}

func (me *Package) getConnectionUMLString(nameOfPackageDependingOnMe string) string {
	if me.hiddenInUml || !me.GeneratePackageToPackageLinks {
		return ""
	}
	var uml string

	uml += nameOfPackageDependingOnMe + " --> " + me.Name + "\n"

	return uml
}
