package converter

type messagePart interface {
	// connect with all message parts this depends on, it accepts it's package and the list of all packages, so it can add inter package dependencies
	connectToDep(*Package, *[]Package)
	// returns ready plant UML string
	generateUMLString() string
	// gets the name of the message part
	getName() string
	// returns UML string having in mind if the message part is hidden and needs to be a field
	getConnectionFromUMLString(connectionFromClassName, nameOfConnection string, collection bool, must bool) string
	// sets if the parts needs to be represented as fields in other classes and not as class dependency (used for libraries)
	setHiddenParameter(bool)
	// used in case you want to generate right after one recursive generation has passed
	resetGeneratedFlag()
}
