package converter

import (
	"fmt"
	"strings"
)

type oneOf struct {
	name         string       // generated name it is the message owning the oneOf + line of start of oneOf in the message + "oneof"
	fields       []oneOfField // the possible selection for the oneof
	connected    bool         // flag for connectivity so we can avoid stack overflow in case there is a circular dependency in the code
	hiddenInUml  bool         // this flag is linked to the owning message and managed by it, as each one of is a part of the ownind message
	generatedUML bool         // flag for recursive generation tracking
}

type oneOfField struct {
	Type                  string      // the type of the possibility (as message or enum class)
	name                  string      // the name of the connection
	packageName           string      // the name of the package owning the class, in case it was available in the IDL
	id                    int         // the IDL id read
	messagePartConnection messagePart // interface to the part representing the class (Type) of the possibility
}

func (me *oneOf) connectToDep(ownPackage *Package, availablePackages *[]Package) {

	if me.connected {
		return
	}

	me.connected = true

	for index := range me.fields {
		memberToConnect := &me.fields[index]
		if memberToConnect.packageName != "" {

			// connect own and dependent package
			ownPackage.ConnectTo(memberToConnect.packageName, availablePackages)

			// search in dependent package for message part we are looking for by name
			var partFoundInPackage bool
			for i := 0; i < len(*availablePackages); i++ {
				packageToLookIn := (*availablePackages)[i]
				if packageToLookIn.Name == memberToConnect.packageName {
					// we found the correct package now search for the message part
					for i := 0; i < len(packageToLookIn.messageParts); i++ {
						messagePartToCheck := packageToLookIn.messageParts[i]
						if messagePartToCheck.getName() == memberToConnect.Type {
							// we found the messagePart we are looking for we add it to the message dependencies
							memberToConnect.messagePartConnection = messagePartToCheck

							partFoundInPackage = true
							// we found what we care for we should break the cycle
							break
						}
					}
					if !partFoundInPackage {
						panic(fmt.Sprintf("Message part reference not found in remote package, name looking for: %s in package: %s", memberToConnect.Type, memberToConnect.packageName))
					}
					// package was found no need for more searching
					break
				}
			}
		} else if !isTypeBasic(memberToConnect.Type) {

			var partFoundInPackage bool
			// the messagePart is supposed to be in our package so we search for it
			for index := range ownPackage.messageParts {
				messagePartToCheck := ownPackage.messageParts[index]
				if messagePartToCheck.getName() == memberToConnect.Type {
					// messagePart is found so we can connect to it
					memberToConnect.messagePartConnection = messagePartToCheck
					partFoundInPackage = true
					break
				}
			}

			for index := range *availablePackages {
				packageToLookIn := (*availablePackages)[index]
				for indexInPackage := range packageToLookIn.messageParts {
					messagePartToCheck := packageToLookIn.messageParts[indexInPackage]
					if messagePartToCheck.getName() == memberToConnect.Type {
						// we found the messagePart we are looking for we add it to the message dependencies
						memberToConnect.messagePartConnection = messagePartToCheck

						partFoundInPackage = true
						// we need to connect to the unexpected package
						if ownPackage.Name != packageToLookIn.Name {
							ownPackage.ConnectTo(packageToLookIn.Name, availablePackages)
						}
						// we found what we care for we should break the cycle
						break
					}
				}
			}

			if !partFoundInPackage {
				panic(fmt.Sprintf("Message part reference not found in own package, name looking for: %s in package: %s", memberToConnect.Type, ownPackage.Name))
			}
		}
	}

	for i := 0; i < len(me.fields); i++ {
		if !isTypeBasic(me.fields[i].Type) {
			messagePartToConnet := me.fields[i].messagePartConnection
			messagePartToConnet.connectToDep(ownPackage, availablePackages)
		}
	}
}

func (me *oneOf) getConnectionFromUMLString(connectionFromClassName, nameOfConnection string, collection bool, must bool) string {
	if me.hiddenInUml {
		return ""
	}

	var uml string

	uml += connectionFromClassName + " \"1\" "

	if must {
		uml += "*-- "
	} else {
		uml += "o-- "
	}

	if collection {
		uml += " \"many\" "
	} else {
		uml += " \"1\" "
	}

	uml += me.name + " : " + nameOfConnection + "\n"

	return uml
}

func (me *oneOf) setHiddenParameter(hiddenInUmlRequest bool) {
	me.hiddenInUml = hiddenInUmlRequest
}

func (me *oneOf) resetGeneratedFlag() {
	me.generatedUML = false
}

func (me *oneOf) generateUMLString() string {
	var uml string

	if me.generatedUML {
		return ""
	}

	me.generatedUML = true

	for index := range me.fields {
		member := me.fields[index]
		if !isTypeBasic(member.Type) {
			uml += member.messagePartConnection.generateUMLString()
		} else {
			uml += me.name + " : " + member.Type + " " + member.name + "\n"
		}
	}

	for index := range me.fields {
		member := me.fields[index]
		if !isTypeBasic(member.Type) {
			// we need to generate as the dependency wants
			// (this allows a specific note to hide or present itself as fields, instead of dependencies)
			uml += member.messagePartConnection.getConnectionFromUMLString(me.name, member.name, false, false)
		}
	}
	return uml
}

func (me oneOf) getName() string {
	return me.name
}

func makeOneOfFromString(data, parentPrefix string) *oneOf {
	var newOneOf oneOf

	rawDataLines := strings.Split(data, "\n")
	for _, rawLine := range rawDataLines {
		// check for a heading
		var bufferForReadingName string
		n, err := fmt.Sscanf(rawLine, "oneof %s", &bufferForReadingName)
		if err == nil && n == 1 {
			newOneOf.name = parentPrefix + bufferForReadingName
		}
		// check for element
		var connectionName, connectionType string
		var connectionID int
		n, err = fmt.Sscanf(rawLine, "%s %s = %d;", &connectionType, &connectionName, &connectionID)
		if err == nil && n != 1 {
			var newField oneOfField

			newField.Type = connectionType
			newField.name = connectionName
			newField.id = connectionID

			if strings.Contains(newField.Type, ".") {
				names := strings.Split(newField.Type, ".")
				newField.Type = names[1]
				newField.packageName = names[0]
			}

			newOneOf.fields = append(newOneOf.fields, newField)
		}
	}

	return &newOneOf
}
