package converter

import (
	"fmt"
	"strings"
)

// Enum holds the data that was parsed from the .proto file
type Enum struct {
	name          string
	possibilities []enumPossibility
	hiddenInUml   bool
	generatedUML  bool
}

type enumPossibility struct {
	name string
	id   int
}

func (me Enum) connectToDep(ownPackage *Package, availablePackages *[]Package) {
	// Enum is not capable of dependencies
}

func (me *Enum) getConnectionFromUMLString(connectionFromClassName, nameOfConnection string, collection bool, must bool) string {
	var uml string

	if me.hiddenInUml {
		uml += connectionFromClassName + " : " + me.name + " " + nameOfConnection + "\n"
		return uml
	}

	uml += connectionFromClassName + " \"1\" "

	if must {
		uml += "*-- "
	} else {
		uml += "o-- "
	}

	if collection {
		uml += " \"many\" "
	} else {
		uml += " \"1\" "
	}

	uml += me.name + " : " + nameOfConnection + "\n"

	return uml
}

func (me Enum) getName() string {
	return me.name
}

func (me *Enum) setHiddenParameter(hiddenInUmlRequest bool) {
	me.hiddenInUml = hiddenInUmlRequest
}

func (me *Enum) resetGeneratedFlag() {
	me.generatedUML = false
}

func (me *Enum) generateUMLString() string {
	var uml string

	if me.generatedUML {
		return ""
	}

	me.generatedUML = true

	uml += "enum " + me.name + " {\n"

	for _, item := range me.possibilities {
		uml += item.name + "\n"
	}

	uml += "}\n"

	return uml
}

func makeEnumFromString(data string) Enum {
	var newEnum Enum

	fmt.Sscanf(data, "enum %s", &newEnum.name)

	dataInLines := strings.Split(data, "\n")

	for _, line := range dataInLines {
		var readPossibility enumPossibility
		n, err := fmt.Sscanf(line, "%s = %d", &readPossibility.name, &readPossibility.id)

		if n == 2 && err == nil {
			newEnum.possibilities = append(newEnum.possibilities, readPossibility)
		}
	}

	return newEnum
}
