package converter

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Message holds a single proto message data
type Message struct {
	name         string          // message name or class name in generation
	members      []messageMember // all fields of the message
	hiddenInUml  bool            // should it be hidden in the UML output
	connected    bool            // has it started to connect to dependencies (this is used in case of circular dependency is in the messages)
	generatedUML bool            // needed for recursive generation as the tree can have several dependencies to a message
}

type messageMember struct {
	name                  string      // the name of the field as it is in the class
	packageName           string      // the package name with which the message is declared in the IDL in case it is
	memberTypeName        string      // the name of the message it is connected to (or the class of the member)
	id                    int         // the IDL ID as in the IDL file
	messagePartConnection messagePart // interface to the message/enum/oneof
	collection            bool        // in case there was a repeat in the IDL this will ne true
	isBasicType           bool        // in case it is a basic type it will be marked here and it will be generated as a field and not as dependency
	must                  bool        // used for generating compositions, for now with oneOf alone
}

func findClosingBracket(data string, startIndex int) int {

	bracketCount := -1
	for i := 0; true; i++ {
		char := data[startIndex+i]
		if char == '{' {
			if bracketCount == -1 {
				bracketCount = 1
			} else {
				bracketCount++
			}
		} else if char == '}' {
			bracketCount--
			if bracketCount == 0 {
				return startIndex + i + 1
			}
		}
	}
	return startIndex
}

func makeNewMessageFromFileAndStartOfMessage(file string, indexes []int) Message {
	var newMessage Message

	regExFound := file[indexes[0]:indexes[1]]

	fmt.Sscanf(regExFound, "message %s", &newMessage.name)

	rawBody := file[indexes[0]:findClosingBracket(file, indexes[0])]
	leftOverBody := rawBody

	// Find all oneOf types and handle them first
	oneOfRegex := regexp.MustCompile(`oneof\W*[a-zA-Z_]*\W*{`)

	if oneOfRegex.MatchString(rawBody) {
		oneOfStartPoints := oneOfRegex.FindAllStringIndex(rawBody, -1)
		for _, startOfOneOf := range oneOfStartPoints {
			oneOfData := rawBody[startOfOneOf[0]:findClosingBracket(rawBody, startOfOneOf[0])]
			newOneOf := makeOneOfFromString(oneOfData, newMessage.name+strconv.Itoa(startOfOneOf[0]))
			leftOverBody = strings.Replace(rawBody, oneOfData, "", 1)

			var newMember messageMember
			newMember.memberTypeName = "oneof"
			newMember.name = newOneOf.name
			newMember.id = -1
			newMember.collection = false
			newMember.messagePartConnection = newOneOf
			newMember.isBasicType = false

			newMessage.members = append(newMessage.members, newMember)
		}
	}

	// Handle repeated and non repeated fields
	rawDataLines := strings.Split(leftOverBody, "\n")
	for _, rawLine := range rawDataLines {
		var messagePartName, connectionName, specialPart string
		var id int
		// check for a simple message connection
		n, err := fmt.Sscanf(rawLine, "%s %s = %d;", &messagePartName, &connectionName, &id)
		if err == nil && n == 3 {
			var newMember messageMember
			newMember.memberTypeName = messagePartName
			newMember.name = connectionName
			newMember.id = id
			newMember.collection = false

			if strings.Contains(newMember.memberTypeName, ".") {
				names := strings.Split(newMember.memberTypeName, ".")
				newMember.packageName = names[0]
				newMember.memberTypeName = names[1]
			}

			newMember.isBasicType = isTypeBasic(newMember.memberTypeName)

			newMessage.members = append(newMessage.members, newMember)
		}
		// check for a repeated connection
		n, err = fmt.Sscanf(rawLine, "%s %s %s = %d;", &specialPart, &messagePartName, &connectionName, &id)
		if err == nil && n == 4 && specialPart == "repeated" {
			var newMember messageMember
			newMember.memberTypeName = messagePartName
			newMember.name = connectionName
			newMember.id = -1
			newMember.collection = true

			if strings.Contains(newMember.memberTypeName, ".") {
				names := strings.Split(newMember.memberTypeName, ".")
				newMember.packageName = names[0]
				newMember.memberTypeName = names[1]
			}

			newMember.isBasicType = isTypeBasic(newMember.memberTypeName)

			newMessage.members = append(newMessage.members, newMember)
		}
	}
	return newMessage
}

func (me *Message) getConnectionFromUMLString(connectionFromClassName, nameOfConnection string, collection bool, must bool) string {
	var uml string

	if me.hiddenInUml {
		uml += connectionFromClassName + " : " + me.name + " " + nameOfConnection + "\n"
		return uml
	}

	uml += connectionFromClassName + " \"1\" "

	if must {
		uml += "*-- "
	} else {
		uml += "o-- "
	}

	if collection {
		uml += " \"many\" "
	} else {
		uml += " \"1\" "
	}

	uml += me.name + " : " + nameOfConnection + "\n"

	return uml
}

func (me *Message) generateUMLString() string {
	var uml string

	if me.hiddenInUml || me.generatedUML {
		return ""
	}

	me.generatedUML = true

	for index := range me.members {
		member := me.members[index]
		if !member.isBasicType {
			uml += member.messagePartConnection.generateUMLString()
		}
	}

	uml += "class " + me.name + "\n"

	for index := range me.members {
		member := me.members[index]
		if member.isBasicType {
			// in case we have a basic type we should generate as a field
			uml += me.name + " : " + member.memberTypeName + " " + member.name + "\n"
		} else {
			// in case we have a dependency we need to generate as the dependency wants
			// (this allows a specific note to hide or present itself as fields, instead of dependencies)
			uml += member.messagePartConnection.getConnectionFromUMLString(me.name, member.name, member.collection, member.must)
		}
	}

	return uml
}

func (me *Message) connectToDep(ownPackage *Package, availablePackages *[]Package) {

	if me.connected {
		return
	}

	me.connected = true

	for index := range me.members {
		memberToConnect := &me.members[index]
		if memberToConnect.messagePartConnection != nil {
			continue
		}

		if memberToConnect.packageName != "" {

			// connect own and dependent package
			if ownPackage.Name != memberToConnect.packageName {
				ownPackage.ConnectTo(memberToConnect.packageName, availablePackages)
			}

			// search in dependent package for message part we are looking for by name
			var partFoundInPackage bool
			for i := 0; i < len(*availablePackages); i++ {
				packageToLookIn := (*availablePackages)[i]
				if packageToLookIn.Name == memberToConnect.packageName {
					// we found the correct package now search for the message part
					for i := 0; i < len(packageToLookIn.messageParts); i++ {
						messagePartToCheck := packageToLookIn.messageParts[i]
						if messagePartToCheck.getName() == memberToConnect.memberTypeName {
							// we found the messagePart we are looking for we add it as member dependency
							memberToConnect.messagePartConnection = messagePartToCheck

							partFoundInPackage = true
							// we found what we care for we should break the cycle
							break
						}
					}
					if !partFoundInPackage {
						panic(fmt.Sprintf("Message part reference not found in remote package, name looking for: %s in package: %s", memberToConnect.memberTypeName, memberToConnect.packageName))
					}
					// package was found no need for more searching
					break
				}
			}
		} else if !isTypeBasic(memberToConnect.memberTypeName) {

			var partFoundInPackage bool
			// the messagePart is supposed to be in our package so we search for it
			for i := 0; i < len(ownPackage.messageParts); i++ {
				messagePartToCheck := ownPackage.messageParts[i]
				if messagePartToCheck.getName() == memberToConnect.memberTypeName {
					// messagePart is found so we can connect to it
					memberToConnect.messagePartConnection = messagePartToCheck

					partFoundInPackage = true
				}
			}

			for i := 0; i < len(*availablePackages); i++ {
				packageToLookIn := (*availablePackages)[i]
				for i := 0; i < len(packageToLookIn.messageParts); i++ {
					messagePartToCheck := packageToLookIn.messageParts[i]
					if messagePartToCheck.getName() == memberToConnect.memberTypeName {
						// we found the messagePart we are looking for we add it to the message dependencies
						memberToConnect.messagePartConnection = messagePartToCheck

						partFoundInPackage = true
						// we need to connect to the unexpected package

						if ownPackage.Name != packageToLookIn.Name {
							ownPackage.ConnectTo(packageToLookIn.Name, availablePackages)
						}
						// we found what we care for we should break the cycle
						break
					}
				}
			}

			if !partFoundInPackage {
				panic(fmt.Sprintf("Message part reference not found in own package, name looking for: %s in package: %s", memberToConnect.memberTypeName, ownPackage.Name))
			}
		}
	}

	for i := 0; i < len(me.members); i++ {
		messagePartToConnet := me.members[i].messagePartConnection
		if !me.members[i].isBasicType {
			messagePartToConnet.connectToDep(ownPackage, availablePackages)
		}
	}
}

func isTypeBasic(typeName string) bool {
	switch typeName {
	case "bool":
		fallthrough
	case "string":
		fallthrough
	case "uint8":
		fallthrough
	case "uint16":
		fallthrough
	case "uint32":
		fallthrough
	case "uint64":
		fallthrough
	case "float":
		fallthrough
	case "int8":
		fallthrough
	case "int16":
		fallthrough
	case "int32":
		fallthrough
	case "int64":
		return true
	default:
		return false
	}
}
func (me Message) getName() string {
	return me.name
}

func (me *Message) setHiddenParameter(hiddenInUmlRequest bool) {
	me.hiddenInUml = hiddenInUmlRequest

	// as all the one of types are only connected to the owning class we need to set their hidden flag from the message that owns the one of
	for index := range me.members {
		member := me.members[index]
		if member.memberTypeName == "oneof" {
			member.messagePartConnection.setHiddenParameter(hiddenInUmlRequest)
		}
	}
}

func (me *Message) resetGeneratedFlag() {
	me.generatedUML = false

	// as all the one of types are only connected to the owning class we need to set their generated flag from the message that owns the one of
	for index := range me.members {
		member := me.members[index]
		if member.memberTypeName == "oneof" {
			member.messagePartConnection.resetGeneratedFlag()
		}
	}
}
