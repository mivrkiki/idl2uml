package converter

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// Project class stores the data for the IDL 2 UMl project
type Project struct {
	Name                string    // the name of the project (it will be used in the UML generation)
	ProtoFilesInStrings []string  // All the proto files that were found in the root directory recursively in string format
	packages            []Package // all packages found in the proto files after parsing
}

func (me *Project) walkAndReadFileSkipDir(path string, info os.FileInfo, err error) error {
	if err != nil {
		fmt.Println(err)
		return nil
	}
	if info.IsDir() {
		return nil
	}
	if filepath.Ext(path) == ".proto" {
		dataFromFile, err := ioutil.ReadFile(path)
		if err == nil {
			me.ProtoFilesInStrings = append(me.ProtoFilesInStrings, string(dataFromFile))
			return nil
		}
		return err
	}
	return nil
}

// LoadFilesFromRoot loads all .proto files as strings in the project member ProtoFilesInStrings
func (me *Project) LoadFilesFromRoot(root string) error {
	me.Name = filepath.Base(root)
	err := filepath.Walk(root, me.walkAndReadFileSkipDir)
	return err
}

func getPackageNameFromFile(file string) string {
	var packagesPrefix, packageName string
	packageName = "NoPackageNameFound"
	packageNameRegEx := regexp.MustCompile(`package\W+[^;]+;`)

	if packageNameRegEx.MatchString(file) {
		loc := packageNameRegEx.FindStringIndex(file)

		nameString := file[loc[0]:loc[1]]

		fmt.Sscanf(nameString, "package %s", &packagesPrefix)

		packagesPrefix = strings.Replace(packagesPrefix, ";", "", 1)

		packages := strings.Split(packagesPrefix, ".")
		packageName = packages[len(packages)-1]
	}
	return packageName
}

// packageInSlice will return a pointer to a package with the same name, or pointer to a new package in the slice with the name given
func packageInSlice(packages *[]Package, nameToSearchFor string) *Package {
	// in case we have the package return a pointer to it
	for index := 0; index < len(*packages); index++ {
		if (*packages)[index].Name == nameToSearchFor {
			return &(*packages)[index]
		}
	}

	// we do not have it, we need to add it
	var newPackage Package
	newPackage.Name = nameToSearchFor
	*packages = append((*packages), newPackage)

	// we return the last item in the slice as we just added it
	return &(*packages)[len((*packages))-1]
}

func (me *Project) loadEnumsFromFiles() {
	commentsRegex := regexp.MustCompile(`[ 	]*[/][/][^\n]*\n`)
	enumRegex := regexp.MustCompile(`enum *([a-zA-Z_]*)\W*\n*\w*{(\n*(?:\W*[a-zA-Z_0-9]* *= *[0-9]*\W*;\W*\n*)*\n*)}`)

	// Enums reading
	for _, fileString := range me.ProtoFilesInStrings {

		packageName := getPackageNameFromFile(fileString)
		packageToLoadIn := packageInSlice(&me.packages, packageName)

		if commentsRegex.MatchString(fileString) {
			fileString = commentsRegex.ReplaceAllLiteralString(fileString, "\n")
		}
		if enumRegex.MatchString(fileString) {
			enumData := enumRegex.FindAllString(fileString, -1)

			for _, enumDataIt := range enumData {
				newEnum := makeEnumFromString(enumDataIt)
				packageToLoadIn.messageParts = append(packageToLoadIn.messageParts, &newEnum)
			}
		}
	}

}

func (me *Project) loadMessagesFromFiles() {
	commentsRegex := regexp.MustCompile(`[ 	]*[/][/][^\n]*\n`)
	messageStartRegEx := regexp.MustCompile(`message *(?:[a-zA-Z_]*)\W*{`)

	// Messages reading
	var countMessages int
	for _, fileString := range me.ProtoFilesInStrings {

		packageName := getPackageNameFromFile(fileString)
		packageToLoadIn := packageInSlice(&me.packages, packageName)

		if commentsRegex.MatchString(fileString) {
			fileString = commentsRegex.ReplaceAllLiteralString(fileString, "\n")
		}
		if messageStartRegEx.MatchString(fileString) {
			messageStartIndexes := messageStartRegEx.FindAllStringIndex(fileString, -1)

			countMessages += len(messageStartIndexes)

			for _, messageStartIndex := range messageStartIndexes {
				newMessage := makeNewMessageFromFileAndStartOfMessage(fileString, messageStartIndex)

				packageToLoadIn.messageParts = append(packageToLoadIn.messageParts, &newMessage)
			}
		}
	}
}

// ParseFilesData parses the data from the strings of the files and makes the messages and enums available
func (me *Project) ParseFilesData() error {

	me.loadEnumsFromFiles()

	me.loadMessagesFromFiles()

	me.connectTreeOfDependencies()

	return nil
}

// GeneratedPackageDiagram returns a UML string with a complete diagram of all packages in the project and their dependencies
func (me *Project) GeneratedPackageDiagram() string {
	var uml string

	uml += "@startuml " + me.Name + "_PackageDiagram\n"

	for _, packageToGenerate := range me.packages {
		uml += packageToGenerate.GeneratePackageWithDependenciesInUML()
	}

	uml += "@enduml"

	return uml
}

func (me *Project) connectTreeOfDependencies() {

	for i := 0; i < len(me.packages); i++ {
		(me.packages[i]).ConnectMessages(&me.packages)
	}
}

// GeneratePackageMessageDiagram returns a Plant UML string with packages and classes
func (me *Project) GeneratePackageMessageDiagram() string {
	var uml string

	uml += "@startuml " + me.Name + "\n"

	for i := 0; i < len(me.packages); i++ {
		packageForGeneration := &(me.packages[i])
		packageForGeneration.ResetGeneratedFlag()
	}

	for i := 0; i < len(me.packages); i++ {
		packageForGeneration := &(me.packages[i])
		uml += packageForGeneration.GeneratePackageAndMessagesInUml()
	}

	uml += "@enduml"

	return uml
}

// GenerateUmlForMessage generates just the dependencies of this message as a UML string
func (me *Project) GenerateUmlForMessage(messageName string) string {
	var messageWeNeedToGenerate *messagePart

	// make sure nothing is hidden so reset all hidden flags to false
	// and try to get a pointer to the message we need to generate
	for index := range me.packages {
		packageToUnHide := me.packages[index]
		packageToUnHide.ResetGeneratedFlag()

		for index := range packageToUnHide.messageParts {
			messageCheck := packageToUnHide.messageParts[index]
			if messageCheck.getName() == messageName {
				messageWeNeedToGenerate = &messageCheck
			}
		}
	}

	var uml string

	uml += "@startuml " + me.Name + "\n"

	uml += (*messageWeNeedToGenerate).generateUMLString()

	uml += "@enduml"

	return uml
}

// HidePackagesAndMessages hides the packages it finds with the names given, it is run recursively to all parts of the packages being hidden
func (me *Project) HidePackagesAndMessages(packagesToHide []string) {
	for _, nameToHide := range packagesToHide {
		var isPackageName bool
		for index := range me.packages {
			if me.packages[index].Name == nameToHide {
				me.packages[index].SetHideFlag(true)
				isPackageName = true
			}
		}

		if !isPackageName {
			for index := range me.packages {
				me.packages[index].HideMessage(nameToHide)
			}
		}
	}
}

// SetHidePackageRelations sets all packages to not generate connections to other packages
func (me *Project) SetHidePackageRelations(hide bool) {
	for index := range me.packages {
		me.packages[index].GeneratePackageToPackageLinks = hide
	}
}

// HideAllButGiven hides all other packages but the one given
func (me *Project) HideAllButGiven(packageName string) {
	for index := range me.packages {
		if me.packages[index].Name != packageName {
			me.packages[index].SetHideFlag(true)
		}
	}
}
