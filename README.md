# IDL to UML generator
This tool analyzes a whole IDL project and builds a dependency tree including packages, messages and enums. Then it generates a PlantUml diagram based on the tree it created from parsing the IDL code.

## Why?
While initially introducing a person to a relatively large IDL project there was the obvious need of diagrams that showed the packages, their relations and later the messages in the packages, and when trying to understand the code a message dependency diagram was needed. So after a brief search there were a few projects that generated UML from IDl but none of them could go through a whole project and give us just the message we care for with it's dependencies. So in the time it was given to get familiar with the project and considering the scale of the project, the goal was mixed with making this tool (in a few days) and generate the needed diagrams so introduction to a large IDL project is faster (hopefully including the initial introduction).

## Output
The output is of 3 types and with different goals:
 - Package diagram - displays all packages that were found in the project and the dependencies between them, the dependencies come from the tree of dependencies of the messages and enums
 - Package Message diagram - displays the data in the package diagram plus all messages (represented as classes) in a single diagram, it has the option to remove package to package relations so dependencies between messages from different packages become clearer.
 - Message diagram - displays a root message and all it's dependencies with their dependencies down to basic types like int, float, string...

## Message representation
Messages a represented as a class in the UML. In the tree they can have dependency to other messages, enums and oneOf messages. Repeated fields are handled as well.

## Enum representation
Enums are represented as an Enum as UML supports enums. Values are represented only by names, although in the data tree you can find the values as well.

## One of representation
One of fields are represented as a composition to a special class that name is  (message name + line of oneOf measured from message start + oneOf string). The possibilities are represented as aggregations of type 1 to 1, this way we are trying to show that for sure there will be a oneOf any of this types.

## repeated representation
Repeated fields are presented as 1 to many aggregation

## Any representation
Any will be threaded as a normal dependency to external message

## Packages representation
Currently the tool works with just one level of packages, package inside a package is not yet supported

# Install

You can just run go get:
```
go get github.com/devolo/idl2uml
```

In the releases you can find precompiled binaries for Linux and Windows.

# Usage
```
Usage of idl2uml:
  -diagram string
        diagram type, [package | packageMessage | message] (caps ignored) (default "package")
  -hidden string
        list of hidden packages or messages. Hidden dependencies will be displayed as fields and not aggregations / compositions in classes and packages will not be in the UML along with dependencies to them
  -message string
        message name (caps sensitive) to be used as root for class diagram (usable only in message diagram)
  -o string
        output file path with extension
  -package string
        a single package to be displayed, all other will be hidden, works only with packageMessage diagram
  -packageLinks
        defines if the package to package links are wanted in the output diagram, available only for packageMessage diagram, in package diagram it is always true
  -path string
        path to the IDL root folder, can be absolute and relative to execution folder, if not set it looks in the execution folder
```

## Examples
Current folder (recursive searched for proto files) with all packages and messages
```
idl2uml -diagram=packagemessage
```
Package only diagram
```
idl2uml -path="\idl" -diagram=package
```
Package diagram with messages in packages
```
idl2uml -path="\idl" -diagram=packageMessage
```
Package diagram with messages in packages, with no package to package relations, and with package lib as library
```
idl2uml -path="\idl" -diagram=packageMessage -hidden=lib -packageLinks=false
```
Message diagram for message1
```
idl2uml -path="\idl" -diagram=message -message=message1
```